


%Waiting for accel on XY plane
%Computing Gravity component on X and Y car frame before Car accelerates

%--Find out all instances of stop and then accelerate in the data---------%

StopDetected = 0;

XYPlaneAcc = XCar.*XCar + YCar.*YCar;
XYPlaneAcc = sqrt(XYPlaneAcc);

StopDetectArr = [];
StartDetectArr = [];

for i=1:nrow
    
    if ((data(i,SpeedCol) == 0) && (StopDetected == 0))
        
        StopDetected = 1;
        XYPlaneAccStationary = mean(XYPlaneAcc(i:i+10));
        XCarStationary = mean(XCar(i:i+10));
        YCarStationary = mean(YCar(i:i+10));
        temp = [i,XCarStationary,YCarStationary,XYPlaneAccStationary ];
        
        StopDetectArr = vertcat(StopDetectArr,temp);
        
     
    end
    
    if StopDetected == 1
       
%        if data(i,SpeedDerivedAccCol) > 0.1
%        
       if XYPlaneAcc(i)-XYPlaneAccStationary > 0.1
            
            StopDetected = 0;
            StartDetectArr = vertcat(StartDetectArr,i);
            
      end
    end
    
end


%-------------------------------------------------------------------------%

%Compute Yaw from all the instances of acceleration detected above

YawArr = [];

[r,c] = size(StartDetectArr);

for i=1:r
    
r1 = StartDetectArr(i);
r2 = StartDetectArr(i)+20;

XCarArr = XCar(r1:r2);
YCarArr = YCar(r1:r2);
XYCarPlaneAccMean = mean(XYPlaneAcc(r1:r2));

XCarGravity = mean(XCar(r1-20:r1));
YCarGravity = mean(YCar(r1-20:r1));

XYCarGravity = sqrt(XCarGravity*XCarGravity + YCarGravity*YCarGravity);

XCarAvg = mean(XCarArr)-XCarGravity;
YCarAvg = mean(YCarArr)-YCarGravity;


% XCarAvg = mean(XCarArr)-XCarStationary;
% YCarAvg = mean(YCarArr)-YCarStationary;


if (abs(mean(ZCarGyro(r1:r2))) < 5) && (XYCarPlaneAccMean > 0.15)

    YawRad = atan2(-YCarAvg,XCarAvg);

    YawDeg = YawRad*180/M_PI;

    dummy = [XCarAvg YCarAvg YawDeg r1 r2];

    YawArr = vertcat(YawArr,dummy);
end

end

YawArrAbs = abs(YawArr);

YawArrMean = mean(YawArrAbs);
