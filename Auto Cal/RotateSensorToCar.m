

% Rotation script
% Sensor to Car cordinates

%  data = Return_EMC_tech;

M_PI = 3.14159;

% PitchSensorToCarRad = 25.53*M_PI/180;
% RollSensorToCarRad = -90.38*M_PI/180;
% YawSensorToCarRad = 169.95*M_PI/180;
%YawSensorToCarRad = 0;

PitchSensorToCarRad = 0.270*M_PI/180;
RollSensorToCarRad = -88.150*M_PI/180;
YawSensorToCarRad = 0;
%YawSensorToCarRad = -178.60*M_PI/180;

XSensorCol = 5; % Raw accel col 
YSensorCol = 6;
ZSensorCol = 7;

XCarCol = 8;
YCarCol = 9;
ZCarCol = 10;

XSensorColGyro = 11; % Raw Gyro Col
YSensorColGyro = 12;
ZSensorColGyro = 13;

XCarColGyro = 14;
YCarColGyro = 15;
ZCarColGyro = 16;


SpeedDerivedAccCol = 17;
SpeedCol = 18;

[nrow,ncol] = size(data);

XSensor = data(:,XSensorCol); % Raw accel from sensor 
YSensor = data(:,YSensorCol);
ZSensor = data(:,ZSensorCol);

XSensorGyro = data(:,XSensorColGyro); % Raw Gyro from sensor 
YSensorGyro = data(:,YSensorColGyro);
ZSensorGyro = data(:,ZSensorColGyro);

XCar = zeros(nrow,1);
YCar = zeros(nrow,1);
ZCar = zeros(nrow,1);

XCarGyro = zeros(nrow,1);
YCarGyro = zeros(nrow,1);
ZCarGyro = zeros(nrow,1);

% Loop starts here

for i= 1:nrow

% Sensor to Car cordinates Euler Rotation
XCar(i) = cos(YawSensorToCarRad) * cos(PitchSensorToCarRad) * XSensor(i) ...
       +(cos(YawSensorToCarRad) * sin(RollSensorToCarRad) * sin(PitchSensorToCarRad) - cos(RollSensorToCarRad) * sin(YawSensorToCarRad)) * YSensor(i) ...
       +(sin(RollSensorToCarRad) * sin(YawSensorToCarRad) + cos(RollSensorToCarRad) * cos(YawSensorToCarRad) * sin(PitchSensorToCarRad)) * ZSensor(i);
        


YCar(i) = cos(PitchSensorToCarRad) * sin(YawSensorToCarRad) * XSensor(i) ...
       + (cos(RollSensorToCarRad) * cos(YawSensorToCarRad) + sin(RollSensorToCarRad) * sin(YawSensorToCarRad) * sin(PitchSensorToCarRad)) * YSensor(i) ...
       + ((cos(RollSensorToCarRad) * sin(YawSensorToCarRad) * sin(PitchSensorToCarRad)) - (cos(YawSensorToCarRad) * sin(RollSensorToCarRad))) * ZSensor(i);



ZCar(i) = -sin(PitchSensorToCarRad) * XSensor(i) ...
       + (cos(PitchSensorToCarRad) * sin(RollSensorToCarRad)) * YSensor(i) ...
       + (cos(RollSensorToCarRad) * cos(PitchSensorToCarRad)) * ZSensor(i);
   
end

for i= 1:nrow

% Sensor to Car Gyro cordinates Euler Rotation
XCarGyro(i) = cos(YawSensorToCarRad) * cos(PitchSensorToCarRad) * XSensorGyro(i) ...
       +(cos(YawSensorToCarRad) * sin(RollSensorToCarRad) * sin(PitchSensorToCarRad) - cos(RollSensorToCarRad) * sin(YawSensorToCarRad)) * YSensorGyro(i) ...
       +(sin(RollSensorToCarRad) * sin(YawSensorToCarRad) + cos(RollSensorToCarRad) * cos(YawSensorToCarRad) * sin(PitchSensorToCarRad)) * ZSensorGyro(i);
        


YCarGyro(i) = cos(PitchSensorToCarRad) * sin(YawSensorToCarRad) * XSensorGyro(i) ...
       + (cos(RollSensorToCarRad) * cos(YawSensorToCarRad) + sin(RollSensorToCarRad) * sin(YawSensorToCarRad) * sin(PitchSensorToCarRad)) * YSensorGyro(i) ...
       + ((cos(RollSensorToCarRad) * sin(YawSensorToCarRad) * sin(PitchSensorToCarRad)) - (cos(YawSensorToCarRad) * sin(RollSensorToCarRad))) * ZSensorGyro(i);



ZCarGyro(i) = -sin(PitchSensorToCarRad) * XSensorGyro(i) ...
       + (cos(PitchSensorToCarRad) * sin(RollSensorToCarRad)) * YSensorGyro(i) ...
       + (cos(RollSensorToCarRad) * cos(PitchSensorToCarRad)) * ZSensorGyro(i);
   
end

% Loop ends here 

XYZCar = horzcat(XCar,YCar,ZCar);
XYZCarGyro =  horzcat(XCarGyro,YCarGyro,ZCarGyro);
 


CarLinearAccelAvg = mean(XCar);
CarLateralAccelAvg = mean(YCar);

CarLinearAccelStd = std(XCar);
CarLateralAccelStd = std(YCar);

XCarRealVsSim = horzcat(data(:,XCarCol),XCar);
plot(XCarRealVsSim,'DisplayName','XCarRealVsSim');
print('-f1','-djpeg','XRealVsSim.jpg');