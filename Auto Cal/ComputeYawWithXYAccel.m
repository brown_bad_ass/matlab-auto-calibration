

%Compute Yaw with XY plane accel


AccelDetected = 0;

XYPlaneAcc = XCar.*XCar + YCar.*YCar;
XYPlaneAcc = sqrt(XYPlaneAcc);

AccelDetectArr = [];

for i=1:nrow
    
    if ((XYPlaneAcc(i) > 0.2) && (AccelDetected == 0))
        
        AccelDetected = 1;
        
    end
    
    if AccelDetected == 1
       
        if XYPlaneAcc(i) < 0.1
            
            AccelDetected = 0;
            AccelDetectArr = vertcat(AccelDetectArr,i);
            
        end
    end
    
end

%Compute Yaw from all the instances of acceleration detected above

YawArr = [];

[r,c] = size(AccelDetectArr);

for i=5:r
    
r1 = AccelDetectArr(i)-20;
r2 = AccelDetectArr(i);

XCarArr = XCar(r1:r2);
YCarArr = YCar(r1:r2);

XCarAvg = mean(XCarArr);
YCarAvg = mean(YCarArr);

YawRad = atan2(-YCarAvg,XCarAvg);

YawDeg = YawRad*180/M_PI;

dummy = [XCarAvg YCarAvg YawDeg];

YawArr = vertcat(YawArr,dummy);
end

YawArrAbs = abs(YawArr);

YawArrMean = mean(YawArrAbs);
