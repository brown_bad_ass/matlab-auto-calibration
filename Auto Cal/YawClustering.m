

% Find cluster in data or getting closer to ground truth

vec = YawArr(:,3);
DominantSign = FindDominantSign(vec);

[r,c] = size(vec);

YawCluster = [];

    
if DominantSign == 1

    for i=1:r
        
        if sign(vec(i)) == 1
            YawCluster = vertcat(YawCluster, vec(i));
        end
        
    end
    
else
    
    for i=1:r
        
        if sign(vec(i)) == -1
            YawCluster = vertcat(YawCluster, vec(i));
        end
        
    end

end


YawClusterAvg = mean(YawCluster);

YawClusterDiff = YawCluster-YawClusterAvg;

[r,c] = size(YawClusterDiff);

YawClusterSim = [];

for i=1:r
  
    if abs(YawClusterDiff(i)) < 10
        YawClusterSim = vertcat(YawClusterSim,YawCluster(i));
    end
        
end


YawClusterSimAvg = mean(YawClusterSim);
