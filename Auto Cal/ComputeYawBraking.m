

% Compute Yaw from Braking

r1 = 4723;
r2 = r1+30;

XCarArr = XCar(r1:r2);
YCarArr = YCar(r1:r2);

XCarAvg = mean(XCarArr)-mean(XCar(r1-10:r1));
YCarAvg = mean(YCarArr)-mean(YCar(r1-10:r1));

YawRad = atan2(-YCarAvg,XCarAvg);

YawDeg = YawRad*180/M_PI;

