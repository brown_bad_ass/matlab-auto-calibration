

function [DominantSign] = FindDominantSign(Arr)

Arr = sign(Arr);

[r,c] = size(Arr);

pos = 0;
neg = 0;
for i=1:r
    
    if Arr(i) == 1
        pos = pos+1;      
    else
        neg = neg+1;
    end
    
end

   if pos > neg
       
       DominantSign = 1;
   else
       DominantSign = -1;
       
   end

end